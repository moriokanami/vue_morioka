// https://jp.vuejs.org/v2/examples/todomvc.html
// StorageAPIを使ったデータの取得・保存の処理

var STORAGE_KEY = 'todos-vuejs-demo'
var todoStorage = {
  fetch: function() {
    var todos = JSON.parse(
      localStorage.getItem(STORAGE_KEY) || '[]'
    )
    todos.forEach(function(todo, index) {
      todo.id = index
    })
    todoStorage.uid = todos.length
    return todos
  },
  save: function(todos) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
  }
}

var options = {
    position: 'top-center',
    duration: 2000,
    fullWidth: true,
    type: 'info'
  }

Vue.use(Toasted, options);

const app = new Vue ({
    el: '#app',
    data: {
        todos:[],
        options: [
            { value: -1, label: 'すべて' },
            { value: 0,  label: '作業中' },
            { value: 1,  label: '完了' }
        ],
        // 選択しているoptionsのvalueを記憶するためのデータ
        // 初期値「-1」つまり「すべて」
        current: -1,
        error: {},
        DatePickerFormat: 'yy-MM-dd',
        endDate: new Date(),
    },
    methods: {
        // TODO追加の処理
        doAdd: function(event, value){
            var comment = this.$refs.comment
            var date = this.$refs.date
            console.log(date)
            if (!comment.value.length) {
                return
            }

            // 新しい状態のオブジェクトをリストへpush

            this.todos.push({
                id: todoStorage.uid++,
                comment: comment.value,
                date: date.value,
                state: 0
            })
            // フォーム要素を空にする
            comment.value = ''
            date.value = ''
        },
        doChangeState: function(item) {
            item.state = item.state ? 0 : 1
        },
        doRemove: function(item) {
            if(confirm('削除しますか？')) {
                var index = this.todos.indexOf(item)
                this.todos.splice(index, 1)
            }
        },
        
    },
    watch: {
        todos: {
            handler: function(todos) {
                todoStorage.save(todos)
            },
            // deepオプションでネストしているデータも監視できる
            deep: true
        }
    },
    created() {
        // インスタンス作成時に自動的にfetch()する
        this.todos = todoStorage.fetch()
    },
    computed: {
        computedTodos: function() {
            // データcurrentが-1ならすべて
            // それ以外ならcurrentとstateが一致するものだけに絞り込む
            return this.todos.filter(function(el) {
                return this.current < 0 ? true : this.current === el.state
            }, this)
        },
        labels() {
            return this.options.reduce(function(a, b) {
                return Object.assign(a, { [b.value]: b.label })
            }, {})
            // キーから見つけやすいように、次のように加工したデータを作成
            // {0: '作業中', 1:'完了', -1:'すべて'}
        }
    },
})